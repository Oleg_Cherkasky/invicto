
<li>
  <a href="#">משכורת והסעות</a>
  <ul class="nav-dropdown">
	<li>
	  <a href="payment-calc.php">חישוב משכורות</a>
	</li>
	<li>
	  <a href="transport.php">הסעות</a>
	</li>
  </ul>
</li>

<li>
  <a href="#">שעות עבודה והדפסת תפסים</a>
  <ul class="nav-dropdown">
	<li>
	  <a href="hours-input.php">הכנסת שעות עבודה</a>
	</li>
	<li>
	  <a href="hours-list.php">עריכת שעות פעילות</a>
	</li>
  </ul>
</li>

<li>
  <a href="#">מחלקות, ערים, ומקצועות</a>
  <ul class="nav-dropdown">
	<li>
	  <a href="department-input.php">ליצור מחלקת חדשה</a>
	</li>
	<li>
	  <a href="department-list.php">עריכת מחלקות קיימות</a>
	</li>
	<li>
	  <a href="profession-input.php">יצירת מקצוע חדש</a>
	</li>
	<li>
	  <a href="profession-list.php">עריכת מקצועות</a>
	</li>
	<li>
	  <a href="city-input.php">יצירת ערים חדשות</a>
	</li>
	<li>
	  <a href="city-list.php">עריכת ערים</a>
	</li>
  </ul>
</li>
<li>
  <a href="#">הנהלת עובדים</a>
  <ul class="nav-dropdown">
	<li>
	  <a href="employee-input.php">להכניס עובד חדש</a>
	</li>
	<li>
	  <a href="employee-list.php">עריכת עובדים קיימים</a>
	</li>
	<li>
	  <a href="signup.php">ליצור משתמש חדש</a>
	</li>
	<li>
	  <a href="user-list.php">עריכת משתמשים</a>
	</li>
  </ul>
</li>