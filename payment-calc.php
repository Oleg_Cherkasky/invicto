<?php require 'head.php';?>
<?php   if (!isset($_SESSION['userKey']))
		header("Location: index.php");
	else {
		$qlist = 7;
	}
?>
<main>
  <div>
  
  <form action="includes/hour.sel.php" method="post">
	<select name="employee-key" size=10>
	<?php
	require 'includes/dbh.inc.php';
	$sql =  "SELECT employeeKey, employeeName FROM employees;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
        echo "<option value=\"".$row['employeeKey']."\">".$row['employeeName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין עובדים</option>";

	$conn->close();
	?>
	</select>
	<br><button name="calc-user" type="submit">לבחור משתמש</button><br>
</form>
  
	<p>תלוש שכר</p>
	
	<?php

	if (isset($_GET['query'])) {
	   	$employee_key = $_GET['query'];
		$totalHours = 0;
		require 'includes/list-queries.php';
		require 'includes/payment-math.php';
		if (sizeof($listE) > 0 && sizeof($dataE) > 0) {
			for($i=0;count($listE)>$i;$i++) {
					$h_start = new DateTime($listE[$i][1]);
					$h_end = new DateTime($listE[$i][2]);
					$totalHours += $h_start->diff($h_end, true)->h;
			}
			echo "Total hours: ".$totalHours."<br>";
			if ($dataE[1])
				echo "Salary: ".$dataE[2];
			else
				echo "Salary: ".$totalHours*$dataE[2];
			
		} else // no data for this user
			echo "אין שעות אצל העובד במאגר";
	}

	?>
	
  </div>
</main>

<?php require 'foot.php';?>