<?php
require "includes/dbh.inc.php";
$listE = [];
if(isset($qtype)) {
	if ($qtype == 1) {
	   	 $sql = "SELECT * FROM employees WHERE employeeKey=".$query.";";
		 
		 if ($conn->connect_error) {
		     die("Connection failed: " . $conn->connect_error);
		 }
		 $result = $conn->query($sql);
		 if ($result->num_rows > 0) {
		     // output data of each row
		     $row = $result->fetch_assoc();
		     array_push($listE, $row['employeeName']);
		     array_push($listE, $row['birthDate']);
		     array_push($listE, $row['beginDate']);
		     array_push($listE, $row['salary']);
		     array_push($listE, $row['professionKey']);
		     array_push($listE, $row['cityId']);
		     array_push($listE, $row['deptKey']);
		     array_push($listE, $row['globalPay']);
		     array_push($listE, $row['employeeKey']);
		}
  	}
	if ($qtype == 2) {
	   	 $sql = "SELECT * FROM departments WHERE deptKey=".$query.";";
		 
		 if ($conn->connect_error) {
		     die("Connection failed: " . $conn->connect_error);
		 }
		 $result = $conn->query($sql);
		 if ($result->num_rows > 0) {
		     // output data of each row
		     $row = $result->fetch_assoc();
		     array_push($listE, $row['deptName']);
		     array_push($listE, $row['secretaryPhone']);
		     array_push($listE, $row['cityId']);
		     array_push($listE, $row['managerKey']);
		     array_push($listE, $row['secretaryKey']);
		     array_push($listE, $row['deptKey']);
		}
	}
	if ($qtype == 3) {
	   	 $sql = "SELECT * FROM professions WHERE professionKey=".$query.";";
		 
		 if ($conn->connect_error) {
		     die("Connection failed: " . $conn->connect_error);
		 }
		 $result = $conn->query($sql);
		 if ($result->num_rows > 0) {
		     // output data of each row
		     $row = $result->fetch_assoc();
		     array_push($listE, $row['profession']);
		     array_push($listE, $row['professionKey']);
		}
	}
	if ($qtype == 4) {
	   	 $sql = "SELECT * FROM cities WHERE cityId=".$query.";";
		 
		 if ($conn->connect_error) {
		     die("Connection failed: " . $conn->connect_error);
		 }
		 $result = $conn->query($sql);
		 if ($result->num_rows > 0) {
		     // output data of each row
		     $row = $result->fetch_assoc();
		     array_push($listE, $row['cityName']);
		     array_push($listE, $row['cityId']);
		}
	}
	if ($qtype == 5) {
	   	 $sql = "SELECT * FROM adminusers WHERE userKey=".$query.";";
		 
		 if ($conn->connect_error) {
		     die("Connection failed: " . $conn->connect_error);
		 }
		 $result = $conn->query($sql);
		 if ($result->num_rows > 0) {
		     // output data of each row
		     $row = $result->fetch_assoc();
		     array_push($listE, $row['manager']);
		     array_push($listE, $row['uidUser']);
		     array_push($listE, $row['emailUser']);
		     array_push($listE, $row['userKey']);
		}
	}
	if ($qtype == 6) {
	   	 $sql = "SELECT * FROM hours WHERE hoursKey=".$query.";";


		 if ($conn->connect_error) {
		     die("Connection failed: " . $conn->connect_error);
		 }
		 $result = $conn->query($sql);
		 if ($result->num_rows > 0) {
		     // output data of each row
		     $row = $result->fetch_assoc();
		     array_push($listE, $row['startHour']);
		     array_push($listE, $row['endHour']);
		     array_push($listE, $row['workDate']);
		     array_push($listE, $row['employeeKey']);
		     array_push($listE, $row['hoursKey']);
		}
	}
}

?>