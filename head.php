<?php
  session_start();
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
  <title>Invicto: Global Performance HR Software</title>
	<!--<link href="https://fonts.googleapis.com/css?family=Righteous&display=swap" rel="stylesheet"> -->
      <link rel="stylesheet" href="css/style.css"> 
	  <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <header>
	  
	  <section class="nav-bar">
  <div class="nav-container">
    <div class="brand">
      <a href=""><img style="padding-top:10px; margin-top:10px" src="img/small-logo.jpg" />   INVICTO</a>
    </div>
    <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#"><span></span></a></div>
      <ul class="nav-list">

		<?php
              if (isset($_SESSION['manager'])) {
                  if ($_SESSION['manager'] == 1)
                      require 'menu-in.php';
                  else
                      require 'menu-un.php';
              }
                      ?>
        
		<li>
          <a href="index.php">דף בית</a>
        </li>

      </ul>
    </nav>
  </div>
</section>
	  
	  <div style="padding-top: 20px; padding-left: 20px">
	    <?php 
		  if (isset($_SESSION['userKey'])) {
			echo '<form action="includes/logout.inc.php" method="post">
		  <button type="submit" name="logout-submit">Logout</button>
		</form>';
			} else {
			  echo '<form action="includes/login.inc.php" method="post">
		  <input type="text" name="mailuid" placeholder="E-Mail/Username">
		  <input type="password" name="pwd" placeholder="Password">
		  <button type="submit" name="login-submit">Login</button>
		</form>';
			}
			
    if (isset($_SESSION['userKey'])) {
		echo '<p>You are logged in</p>';
	} else {
		echo '<p>You are logged out</p>';
	}
	
	//if (isset($_SESSION['empKey']))
	//		echo $_SESSION['empKey'];
  ?>
	  </div>
	</header>
