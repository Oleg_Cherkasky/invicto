
<?php require 'head.php';?>

<main>
  <h2>Signup</h2>
  <?php
  
  if (!isset($_SESSION['userKey']))
	header("Location: index.php");
  
    if (isset($_GET['error'])) {
	  if ($_GET['error'] == "emptyfields") {
		echo "<p>Fill in all fields!</p>";
	  } elseif ($_GET['error'] == "invaliduidmail") {
		echo "<p>Invalid username and e-mail!</p>";
	  } elseif ($_GET['error'] == "invaliduid") {
		echo "<p>Invalid username!</p>";
	  } elseif ($_GET['error'] == "invalidmail") {
		echo "<p>Invalid e-mail!</p>";
	  } elseif ($_GET['error'] == "passwordcheck") {
		echo "<p>Your passwords do not match!</p>";
	  } elseif ($_GET['error'] == "usertaken") {
		echo "<p>Username already taken!</p>";
	  }
	} elseif (isset($_GET['success'])) {
	  echo "<p>Signup successful!</p>";
	}

  ?>
  <form action="includes/hour.inc.php" method="post">
    <input type="text" name="date" placeholder="תאריך">
    <input type="text" name="hstart" placeholder="שעת התחלת המשמרת">
	<input type="text" name="hend" placeholder="שעת סוף המשמרת">
	
	<select name="people">
	<?php
	require 'includes/dbh.inc.php';
	$sql =  "SELECT employeeKey, employeeName FROM employees;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
        echo "<option value=\"".$row['employeeKey']."\">".$row['employeeName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין מקצועות</option>";
	$conn->close();
	?>
	</select>
	
	</br />
    <button type="submit" name="hour-submit">Signup</button>
  </form>
</main>

<?php require 'foot.php';?>