<?php require 'head.php' ;
        if (!isset($_SESSION['userKey']))
		header("Location: index.php");
	if((!isset($_GET['query'])) || $_GET['query'] == '') {
	     header("Location: employee-list.php");
	     exit();
	}
	else {
	     $qtype = 1;
	     $query = $_GET['query'];
     	     require "includes/fetch.php";
	}
?>
<main>

<form action="includes/employee.sel.php" method="post">
	<input type="text" name="wname" value="<?php echo $listE[0] ?>">
	<span> תאריך לידה:</span><input type="date" name="wbirth" value="<?php echo $listE[1] ?>">
	<span> תאריך התחלת עבודה:</span><input type="date" name="wstartdate" value="<?php echo $listE[2] ?>">
	<input type="text" name="wsalary" value="<?php echo $listE[3] ?>">
	
	<span><br />מקצועות:</span>
	<select name="positions">
	<?php
	
	$sql =  "SELECT professionKey, profession FROM professions;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
	  	  if($row['professionKey'] == $listE[4])
		  		echo "<option value=\"".$row['professionKey']."\" selected>".$row['profession']."</option>";
		  else
				echo "<option value=\"".$row['professionKey']."\">".$row['profession']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין מקצועות</option>";

	?>
	</select>
	
	<span>ערים:</span>
	<select name="cities">
	<?php
	$sql =  "SELECT cityId, cityName FROM cities;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
	  	  if($row['cityId'] == $listE[5])
		  	echo "<option value=\"".$row['cityId']."\" selected>".$row['cityName']."</option>";
		  else
		  	echo "<option value=\"".$row['cityId']."\">".$row['cityName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין ערים</option>";

	?>
	</select>
	
	<span>מחלקות:</span>
	<select name="departments">
	<?php
	$sql =  "SELECT deptKey, deptName FROM departments;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
	  	  if($row['deptKey'] == $listE[6])
		  	echo "<option value=\"".$row['deptKey']."\" selected>".$row['deptName']."</option>";
		  else
			echo "<option value=\"".$row['deptKey']."\">".$row['deptName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין מחלקות</option>";
	$conn->close();
	?>
	</select>

	<span>משכורת גלובאלית:</span>
	<select name="globalpay">
		<?php
			if($listE[7] == "1") {
				echo "<option value=1 selected>כן</option>";
				echo "<option value=0>לא</option>";
			} else {
				echo "<option value=1>כן</option>";
				echo "<option value=0 selected>לא</option>";
			}
		?>
		</select>
	
	</br />
    <input type="hidden" name="editid" value="<?php echo $listE[8] ?>">
    <button type="submit" name="cancel">ביטול</button>
    <button type="submit" name="employee-update">לערוך</button>
</form>
</main>
<?php require 'foot.php' ;?>