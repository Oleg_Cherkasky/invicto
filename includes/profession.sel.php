<?php
require "dbh.inc.php";

if (isset($_POST['cancel'])) {
	 header("Location: ../profession-list.php");
	 exit();
}

if (isset($_POST['pro-edit'])) {
   	 if ($_POST['pro-edit'] < 3) {
	        header("Location: ../profession-list.php?error=forbidden");
	  	exit();
	 }
	 header("Location: ../profession-edit.php?query=".$_POST['pro-edit']);
	 exit();
}
if (isset($_POST['pro-delete'])) {
    	 $key = $_POST['pro-delete'];
	   if ($key < 3) {
	        header("Location: ../profession-list.php?error=forbidden");
	  	exit();
	   }
	 //echo $key;
	 $sql = "DELETE FROM professions WHERE professionKey=".$key.";";
	 if ($conn->connect_error) {
	     header("Location: ../profession-list.php?error=".$conn->connect_error);
	     $conn->close();
	     exit();
	 }
	 $result = $conn->query($sql);
	 $conn->close();
	 header("Location: ../profession-list.php?status=deleted");
	 exit();

}

if (isset($_POST['pro-update'])) {
  $pname = $_POST['pname'];
  $pkey = (int)$_POST['editid'];

  if ($pkey < 3) {
     	  header("Location: ../profession-list.php?error=forbidden");
	  exit();
  }
  
  if (empty($pname)) {
	  header("Location: ../profession-list.php?error=emptyfields");
	  exit();
  }
  elseif (!preg_match("/\p{Hebrew}/u", $pname)) {
	  header("Location: ../profession-list.php?error=invalidpname&pname=".$pname);
	  exit();
  }
  else {
	
				$sql = "UPDATE professions SET profession = ? WHERE professionKey = ?;";
				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_bind_param($stmt,"si", $pname, $pkey);
				if (!mysqli_stmt_prepare($stmt, $sql)) {
						header("Location: ../profession-list.php?query=".$deptKey."&error=sqlerror");
						exit();
				}
				else {
					mysqli_stmt_bind_param($stmt,"si", $pname, $pkey);
					mysqli_stmt_execute($stmt);
					header("Location: ../profession-list.php?query=".$deptKey."&edit=successful-edit");
					exit();
				}
	
	mysqli_stmt_close($stmt);
	mysql_close($conn);

}
}

?>