<?php require 'head.php' ;
        if (!isset($_SESSION['userKey']))
		header("Location: index.php");
	if((!isset($_GET['query'])) || $_GET['query'] == '') {
	     header("Location: city-list.php");
	     exit();
	}
	else {
	     $qtype = 4;
	     $query = $_GET['query'];
     	     require "includes/fetch.php";
	}
?>
<main>
  <h2>Signup</h2>
  <?php
  
    if (isset($_GET['error'])) {
	  if ($_GET['error'] == "emptyfields") {
		echo "<p>Fill in all fields!</p>";
	  } elseif ($_GET['error'] == "invaliduidmail") {
		echo "<p>Invalid username and e-mail!</p>";
	  } elseif ($_GET['error'] == "invaliduid") {
		echo "<p>Invalid username!</p>";
	  } elseif ($_GET['error'] == "invalidmail") {
		echo "<p>Invalid e-mail!</p>";
	  } elseif ($_GET['error'] == "passwordcheck") {
		echo "<p>Your passwords do not match!</p>";
	  } elseif ($_GET['error'] == "usertaken") {
		echo "<p>Username already taken!</p>";
	  }
	} elseif (isset($_GET['success'])) {
	  echo "<p>Signup successful!</p>";
	}

  ?>
  <form action="includes/city.sel.php" method="post">
    <input type="text" name="cname" value="<?php echo $listE[0] ?>">
	</br />
<input type="hidden" name="editid" value="<?php echo $listE[1] ?>">
    <button type="submit" name="cancel">ביטול</button>
    <button type="submit" name="city-update">לערוך</button>
  </form>
</main>

<?php require 'foot.php';?>