<?php

if (isset($_POST['departments-submit'])) {
  require "dbh.inc.php";
  
  $dname = $_POST['dname'];
  $sphone = $_POST['secretaryphone'];
  $dcities = (int)$_POST['cities'];
  $dmanager = (int)$_POST['manager'];
  $dsecretary = (int)$_POST['secretary'];
  
  
  if (empty($dname) || empty($sphone) || empty($dcities) || empty($dmanager) || empty($dsecretary)) {
	  header("Location: ../department-input.php?error=emptyfields");
	  exit();
  }
  elseif (!preg_match("/\p{Hebrew}/u", $dname)) {
	  header("Location: ../department-input.php?error=invaliddname&dname=".$dname);
	  exit();
  }
  elseif (!preg_match("/^\d{3}\)?[\s.-]\d{7}$/", $sphone)) {
	  header("Location: ../department-input.php?error=invalidphone&sphone=".$sphone);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $dcities)) {
	  header("Location: ../department-input.php?error=invalidcity&dcities=".$dcities);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $dmanager)) {
	  header("Location: ../department-input.php?error=invalidmanager&dmanager=".$dmanager);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $dsecretary)) {
	  header("Location: ../department-input.php?error=invalidsecretary&dsecretary=".$dsecretary);
	  exit();
  }
  else {
	$sql = "SELECT deptName FROM departments WHERE managerKey=? OR secretaryKey=? OR secretaryPhone=?";
	$stmt = mysqli_stmt_init($conn);
	if (!mysqli_stmt_prepare($stmt, $sql)) {
		header("Location: ../department-input.php?error=sqlerror");
		exit();
	}
	else {
		mysqli_stmt_bind_param($stmt,"sii", $sphone, $dmanager, $dsecretary);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_store_result($stmt);
		$resultCheck = mysqli_stmt_num_rows($stmt);
		if ($resultCheck > 0) {
			header("Location: ../department-input.php?error=identicalmanagersecretary");
			exit();
		}
		else {
				$sql = "INSERT INTO departments (deptName, secretaryPhone, cityId, managerKey,secretaryKey) VALUES (?, ?, ?, ?, ?);";
				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_bind_param($stmt,"ssiii", $dname, $sphone, $dcities, $dmanager, $dsecretary);
				if (!mysqli_stmt_prepare($stmt, $sql)) {
						header("Location: ../department-input.php?error=sqlerror");
						exit();
				}
				else {
					mysqli_stmt_bind_param($stmt,"ssiii", $dname, $sphone, $dcities, $dmanager, $dsecretary);
					mysqli_stmt_execute($stmt);
					header("Location: ../department-input.php?signup=success");
					exit();
				}
		}
	}
	mysqli_stmt_close($stmt);
	mysql_close($conn);
  }
}
else {
	header("Location: ../department-input.php");
	exit();
	
}
?>