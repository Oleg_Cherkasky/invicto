-- MySQL dump 10.18  Distrib 10.3.27-MariaDB, for debian-linux-gnueabihf (armv8l)
--
-- Host: localhost    Database: invicto
-- ------------------------------------------------------
-- Server version	10.3.27-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adminusers`
--

DROP TABLE IF EXISTS `adminusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adminusers` (
  `userKey` int(11) NOT NULL AUTO_INCREMENT,
  `manager` tinyint(1) NOT NULL,
  `employeeKey` int(11) NOT NULL,
  `uidUser` tinytext NOT NULL,
  `emailUser` tinytext NOT NULL,
  `pwdUser` longtext NOT NULL,
  PRIMARY KEY (`userKey`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adminusers`
--

LOCK TABLES `adminusers` WRITE;
/*!40000 ALTER TABLE `adminusers` DISABLE KEYS */;
INSERT INTO `adminusers` VALUES (10,1,3,'davem','davem9@yalla.co.il','$2y$10$pmpCjHiKPciwqPv1jt9bbOCsgU8OXaK3wsN8qb5eQDK8bbJSPfDbW'),(13,0,8,'marina','marin@walla.co.il','$2y$10$8w.5u6bG6Z.LxTyMKbCIHuHjF3gOk.CUOzADT56j91dUisTHgoUUy'),(14,0,2,'dave','daves1@walla.co.il','$2y$10$5C9sStaf.ttv3t4E4eNvK.Zv0iKl9Tnz6TYtn7DOu6WcT1oLOio4.'),(16,0,4,'shimon','shmn@yalla.co.il','$2y$10$d9VF8KMn4fAcx6m/7r4C5uJgjOBO96lIaMK/CP0CuzBshoMbcmPLS');
/*!40000 ALTER TABLE `adminusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `cityId` int(11) NOT NULL AUTO_INCREMENT,
  `cityName` tinytext NOT NULL,
  PRIMARY KEY (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'באר שבע'),(2,'בת ים'),(4,'תל אביב');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `deptKey` int(11) NOT NULL AUTO_INCREMENT,
  `deptName` tinytext NOT NULL,
  `cityId` int(11) NOT NULL,
  `secretaryPhone` tinytext NOT NULL,
  `managerKey` int(11) NOT NULL,
  `secretaryKey` int(11) NOT NULL,
  PRIMARY KEY (`deptKey`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'משאבי אנוש',1,'050-2343358',2,8);
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `employeeKey` int(11) NOT NULL AUTO_INCREMENT,
  `birthDate` tinytext NOT NULL,
  `cityId` int(11) NOT NULL,
  `deptKey` int(11) NOT NULL,
  `employeeName` tinytext NOT NULL,
  `professionKey` int(11) NOT NULL,
  `salary` int(11) NOT NULL,
  `beginDate` tinytext NOT NULL,
  `globalPay` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`employeeKey`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (2,'25/09/1985',1,1,'דוד שינדלר',1,10000,'03/04/2019',1),(3,'25/04/1985',1,1,'דוד מנדל',1,35,'03/04/2019',1),(4,'09/12/1984',1,1,'שמעון פליישמן',3,15000,'03/03/2019',1),(5,'01/02/1994',1,1,'עמית זליג',1,10000,'10/01/2020',1),(8,'24/11/1990',1,1,'מרינה ולדברג',2,30,'10/01/2020',0);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hours`
--

DROP TABLE IF EXISTS `hours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hours` (
  `hoursKey` int(11) NOT NULL AUTO_INCREMENT,
  `employeeKey` int(11) NOT NULL,
  `startHour` tinytext NOT NULL,
  `endHour` tinytext NOT NULL,
  `totalHours` float NOT NULL,
  `workDate` tinytext NOT NULL,
  PRIMARY KEY (`hoursKey`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hours`
--

LOCK TABLES `hours` WRITE;
/*!40000 ALTER TABLE `hours` DISABLE KEYS */;
INSERT INTO `hours` VALUES (4,8,'09:00','17:00',8,'03/30/2021');
/*!40000 ALTER TABLE `hours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professions`
--

DROP TABLE IF EXISTS `professions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professions` (
  `professionKey` int(11) NOT NULL AUTO_INCREMENT,
  `profession` tinytext NOT NULL,
  PRIMARY KEY (`professionKey`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professions`
--

LOCK TABLES `professions` WRITE;
/*!40000 ALTER TABLE `professions` DISABLE KEYS */;
INSERT INTO `professions` VALUES (1,'מנהל/ת'),(2,'מזכיר/מזכירה'),(3,'מהנדס/ת אלקטרוניקה'),(4,'מנהל/ת חשבונות'),(5,'עובד/ת ניקיון');
/*!40000 ALTER TABLE `professions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-13 19:41:51
