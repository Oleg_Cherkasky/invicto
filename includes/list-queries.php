<?php
require "dbh.inc.php";
if(isset($qlist)) {
if($qlist == 1) {
$listE = [];
$sql = "SELECT * FROM employees;";

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    }
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
        while($row = $result->fetch_assoc()) {
		   if ($row["employeeKey"] != $_SESSION['empKey'])
			   array_push($listE, array($row["employeeKey"], $row["employeeName"], $row['birthDate']));
	}
} else {
         array_push($listE, "no data");
}
$conn->close();
}

if($qlist == 2) {
$listE = [];
$sql = "SELECT * FROM departments;";

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    }
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
        while($row = $result->fetch_assoc()) {
		array_push($listE, array($row["deptKey"], $row["deptName"]));
	}
} else {
         array_push($listE, "no data");
}
$conn->close();
}

if($qlist == 3) {
$listE = [];
$sql = "SELECT * FROM professions;";

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    }
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
        while($row = $result->fetch_assoc()) {
		if ($row["professionKey"] > 2)
			array_push($listE, array($row["professionKey"], $row["profession"]));
	}
} else {
         array_push($listE, "no data");
}
$conn->close();
}

if($qlist == 4) {
$listE = [];
$sql = "SELECT * FROM cities;";

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    }
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
        while($row = $result->fetch_assoc()) {
			array_push($listE, array($row["cityId"], $row["cityName"]));
	}
} else {
         array_push($listE, "no data");
}
$conn->close();
}

if($qlist == 5) {
$listE = [];
$sql = "SELECT * FROM adminusers;";

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    }
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
        while($row = $result->fetch_assoc()) {
			array_push($listE, array($row["userKey"], $row["uidUser"], $row["emailUser"]));
	}
} else {
         array_push($listE, "no data");
}
$conn->close();
}

// must be able to filter per employee
if($qlist == 6) {
$listE = [];
$sql = "SELECT * FROM hours WHERE employeeKey = ".$employee_key.";";
$stmt = mysqli_stmt_init($conn);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
        while($row = $result->fetch_assoc()) {
			array_push($listE, array($row["hoursKey"], $row["employeeKey"], $row["workDate"], $row["startHour"], $row["endHour"]));
	}
}
$conn->close();
}

if($qlist == 7) {
$listE = [];
$dataE = [];
$sql = "SELECT * FROM hours WHERE employeeKey = ".$employee_key.";";
$stmt = mysqli_stmt_init($conn);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
        while($row = $result->fetch_assoc()) {
			array_push($listE, array($row["workDate"], $row["startHour"], $row["endHour"]));
		
	}
}

$sql = "SELECT * FROM employees WHERE employeeKey = ".$employee_key.";";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			array_push($dataE, $row["employeeName"]);
			array_push($dataE, $row["globalPay"]);
			array_push($dataE, $row["salary"]);
}
$conn->close();
}

} else {
	header("Location: /index.php");
	exit();
}

if($qlist == 8) {
$listE = [];

if ($row['manager'] == 1) {
$sql = "select employees.cityid, cities.cityName, count(employees.cityId) as emps from employees join cities on cities.cityid=employees.cityid  where cities.cityId=".$_SESSION['city']." group by cityid;";
} else {
$sql = "select employees.cityid, cities.cityName, count(employees.cityId) as emps from employees join cities on cities.cityid=employees.cityid group by cityid;";
}

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    }
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
        while($row = $result->fetch_assoc()) {
			array_push($listE, array($row["cityName"], $row["emps"]));
	}
} else {
         array_push($listE, "no data");
}
$conn->close();
}

?>