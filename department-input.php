
<?php require 'head.php';?>

<main>
  <h2>Signup</h2>
  <?php
  
  if (!isset($_SESSION['userKey']))
	header("Location: index.php");
  
    if (isset($_GET['error'])) {
	  if ($_GET['error'] == "emptyfields") {
		echo "<p>Fill in all fields!</p>";
	  } elseif ($_GET['error'] == "invaliduidmail") {
		echo "<p>Invalid username and e-mail!</p>";
	  } elseif ($_GET['error'] == "invaliduid") {
		echo "<p>Invalid username!</p>";
	  } elseif ($_GET['error'] == "invalidmail") {
		echo "<p>Invalid e-mail!</p>";
	  } elseif ($_GET['error'] == "passwordcheck") {
		echo "<p>Your passwords do not match!</p>";
	  } elseif ($_GET['error'] == "usertaken") {
		echo "<p>Username already taken!</p>";
	  }
	} elseif (isset($_GET['success'])) {
	  echo "<p>Signup successful!</p>";
	}


  ?>
  <form action="includes/department.inc.php" method="post">
    <input type="text" name="dname" placeholder="שם המחלקת">
	<input type="text" name="secretaryphone" placeholder="מספר המטלפון של מזכירה">
	
	<select name="cities">
	<?php
		require 'includes/dbh.inc.php';

	$sql =  "SELECT * FROM cities;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
        echo "<option value=\"".$row['cityId']."\">".$row['cityName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין ערים</option>";

	?>
	</select>
	
		<select name="manager">
	<?php
	$sql =  "SELECT employeeKey, employeeName FROM employees WHERE professionKey=1;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
        echo "<option value=\"".$row['employeeKey']."\">".$row['employeeName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין מחלקות</option>";
	?>
	</select>
	
	<select name="secretary">
	<?php
	$sql =  "SELECT employeeKey, employeeName FROM employees WHERE professionKey=2;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
        echo "<option value=\"".$row['employeeKey']."\">".$row['employeeName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין מחלקות</option>";
	$conn->close();
	?>
	</select>
	
	</br />
    <button type="submit" name="departments-submit">Signup</button>
  </form>
</main>

<?php require 'foot.php';?>