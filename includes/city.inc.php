<?php

if (isset($_POST['city-submit'])) {
  require "dbh.inc.php";
  
  $cname = $_POST['cname'];
  
  
  if (empty($cname)) {
	  header("Location: ../city-input.php?error=emptyfields");
	  exit();
  }//-----------------
  elseif (!preg_match("/\p{Hebrew}/u", $cname)) {
	  header("Location: ../city-input.php?error=invalidcname&cname=".$cname);
	  exit();
  }
  else {
	$sql = "SELECT cityName FROM cities WHERE cityName=?;";
	$stmt = mysqli_stmt_init($conn);
	if (!mysqli_stmt_prepare($stmt, $sql)) {
		header("Location: ../city-input.php?error=sqlerror");
		exit();
	}
	else {
		mysqli_stmt_bind_param($stmt,"s", $cname);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_store_result($stmt);
		$resultCheck = mysqli_stmt_num_rows($stmt);
		if ($resultCheck > 0) {
			header("Location: ../city-input.php?error=identicalcities");
			exit();
		}
		else {
				$sql = "INSERT INTO cities (cityName) VALUES (?);";
				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_bind_param($stmt,"s", $cname);
				if (!mysqli_stmt_prepare($stmt, $sql)) {
						header("Location: ../city-input.php?error=sqlerror");
						exit();
				}
				else {
					mysqli_stmt_bind_param($stmt,"s", $cname);
					mysqli_stmt_execute($stmt);
					header("Location: ../city-input.php?signup=success");
					exit();
				}
		}
	}
	mysqli_stmt_close($stmt);
	mysql_close($conn);
  }
}
else {
	header("Location: ../city-input.php");
	exit();
	
}
?>