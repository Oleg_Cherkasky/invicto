<?php
require "dbh.inc.php";

if (isset($_POST['employee-edit'])) {
	 header("Location: ../employee-edit.php?query=".$_POST['employee-edit']);
	 exit();
}
if (isset($_POST['employee-delete'])) {
   	 $key = $_POST['employee-delete'];
	 //echo $key;
	 $sql = "DELETE FROM employees WHERE employeeKey=".$key.";";
	 if ($conn->connect_error) {
	     header("Location: ../employee-list.php?error=".$conn->connect_error);
	     $conn->close();
	     exit();
	 }
	 $result = $conn->query($sql);
	 $conn->close();
	 header("Location: ../employee-list.php?status=deleted");
	 exit();

}

if (isset($_POST['cancel'])) {
	 header("Location: ../employee-list.php");
	 exit();
}

if (isset($_POST['employee-update'])) {
	   $wname = $_POST['wname'];
  $wbirth = $_POST['wbirth'];
  $wstartd = $_POST['wstartdate'];
  $wsalary = (int)$_POST['wsalary'];
  $profession = (int)$_POST['positions'];
  $city = (int)$_POST['cities'];
  $department = (int)$_POST['departments'];
  $empkey = (int)$_POST['editid'];
  $globalp = "false";
  if ((int)$_POST['globalpay'])
     $globalp = "true";
  
  if (empty($wname) || empty($wbirth) || empty($wstartd)) {
	  header("Location: ../employee-edit.php?query=".$empkey."&error=emptyfields");
	  exit();
  }//-----------------
  elseif (!preg_match("/\p{Hebrew}/u", $wname)) {
	  header("Location: ../employee-edit.php?query=".$empkey."&error=invalidwname&wname=".$wname);
	  exit();
  }
  
  elseif (!preg_match("/^\d{4}[\s.\-][0-1][0-9][\s.\-][0-3][0-9]$/", $wbirth)) {
	  header("Location: ../employee-edit.php?query=".$empkey."&error=invalidphone&wbirth=".$wbirth);
	  exit();
  }
  elseif (!preg_match("/^\d{4}[\s.\-][0-1][0-9][\s.\-][0-3][0-9]$/", $wstartd)) {
	  header("Location: ../employee-edit.php?query=".$empkey."&error=invalidcity&wstartd=".$wstartd);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $wsalary)) {
	  header("Location: ../employee-edit.php?query=".$empkey."&error=invalidmanager&dmanager=".$wsalary);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $profession)) {
	  header("Location: ../employee-edit.php?query=".$empkey."&error=invalidsecretary&profession=".$profession);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $city)) {
	  header("Location: ../employee-edit.php?query=".$empkey."&error=invalidsecretary&city=".$city);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $department)) {
	  header("Location: ../employee-edit.php?query=".$empkey."&error=invalidsecretary&department=".$department);
	  exit();
  }
  else {
	
				$sql = "UPDATE employees SET employeeName = ?, birthDate = ?, beginDate = ?, cityId = ?, deptKey = ?, professionKey = ?, salary = ?, globalPay = ".$globalp." WHERE employeeKey = ?;";
				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_bind_param($stmt,"sssiiiii", $wname, $wbirth, $wstartd, $city, $department, $profession, $wsalary, $empkey);
				if (!mysqli_stmt_prepare($stmt, $sql)) {
						header("Location: ../employee-edit.php?query=".$empkey."&error=sqlerror");
						exit();
				}
				else {
					mysqli_stmt_bind_param($stmt,"sssiiiii", $wname, $wbirth, $wstartd, $city, $department, $profession, $wsalary, $empkey);
					mysqli_stmt_execute($stmt);
					header("Location: ../employee-list.php?query=".$empkey."&edit=successful-edit");
					exit();
				}
	
	mysqli_stmt_close($stmt);
	mysql_close($conn);

}
}

?>