<?php

if (isset($_POST['profession-submit'])) {
  require "dbh.inc.php";
  
  $pname = $_POST['pname'];
  
  
  if (empty($pname)) {
	  header("Location: ../profession-input.php?error=emptyfields");
	  exit();
  }//-----------------
  elseif (!preg_match("/\p{Hebrew}/u", $pname)) {
	  header("Location: ../profession-input.php?error=invalidpname&pname=".$pname);
	  exit();
  }
  else {
	$sql = "SELECT profession FROM professions WHERE profession=?;";
	$stmt = mysqli_stmt_init($conn);
	if (!mysqli_stmt_prepare($stmt, $sql)) {
		header("Location: ../profession-input.php?error=sqlerror");
		exit();
	}
	else {
		mysqli_stmt_bind_param($stmt,"s", $pname);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_store_result($stmt);
		$resultCheck = mysqli_stmt_num_rows($stmt);
		if ($resultCheck > 0) {
			header("Location: ../profession-input.php?error=identicalprofessions");
			exit();
		}
		else {
				$sql = "INSERT INTO professions (profession) VALUES (?);";
				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_bind_param($stmt,"s", $pname);
				if (!mysqli_stmt_prepare($stmt, $sql)) {
						header("Location: ../profession-input.php?error=sqlerror");
						exit();
				}
				else {
					mysqli_stmt_bind_param($stmt,"s", $pname);
					mysqli_stmt_execute($stmt);
					header("Location: ../profession-input.php?signup=success");
					exit();
				}
		}
	}
	mysqli_stmt_close($stmt);
	mysql_close($conn);
  }
}
else {
	header("Location: ../profession-input.php");
	exit();
	
}
?>