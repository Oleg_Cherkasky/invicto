<?php require 'head.php' ;
        if (!isset($_SESSION['userKey']))
		header("Location: index.php");
	if((!isset($_GET['query'])) || $_GET['query'] == '') {
	     header("Location: department-list.php");
	     exit();
	}
	else {
	     $qtype = 2;
	     $query = $_GET['query'];
     	     require "includes/fetch.php";
	}
?>

<main>
  <h2>Signup</h2>

  <form action="includes/department.sel.php" method="post">
    <input type="text" name="dname" value="<?php echo $listE[0] ?>">
	<input type="text" name="secretaryphone" value="<?php echo $listE[1] ?>">
	
	<select name="cities">
	<?php
	$sql =  "SELECT * FROM cities;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
  	  	  if($row['cityId'] == $listE[2])
		  	echo "<option value=\"".$row['cityId']."\" selected>".$row['cityName']."</option>";
		  else
		  	echo "<option value=\"".$row['cityId']."\">".$row['cityName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין ערים</option>";

	?>
	</select>
	
		<select name="manager">
	<?php
	$sql =  "SELECT employeeKey, employeeName FROM employees WHERE professionKey=1;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
  	  	  if($row['managerKey'] == $listE[3])
		  	echo "<option value=\"".$row['employeeKey']."\" selected>".$row['employeeName']."</option>";
		  else
		  	echo "<option value=\"".$row['employeeKey']."\">".$row['employeeName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין מחלקות</option>";
	?>
	</select>
	
	<select name="secretary">
	<?php
	$sql =  "SELECT employeeKey, employeeName FROM employees WHERE professionKey=2;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
  	  	  if($row['secretaryKey'] == $listE[4])
		  	echo "<option value=\"".$row['employeeKey']."\" selected>".$row['employeeName']."</option>";
		  else
		  	echo "<option value=\"".$row['employeeKey']."\">".$row['employeeName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין מחלקות</option>";
	$conn->close();
	?>
	</select>
	
	</br />
<input type="hidden" name="editid" value="<?php echo $listE[5] ?>">
    <button type="submit" name="cancel">ביטול</button>
    <button type="submit" name="department-update">לערוך</button>

  </form>
</main>

<?php require 'foot.php';?>