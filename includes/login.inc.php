<?php

if (isset($_POST['login-submit'])) {
	
	require "dbh.inc.php";
	
	$mailuid = $_POST['mailuid'];
	$password = $_POST['pwd'];
	
	if (empty($mailuid) || empty($password)) {
		header("Location: ../index.php?error=emptyfields");
		exit();
	}
	else {
		$sql = "SELECT * FROM adminusers WHERE uidUser=? OR emailUser=?;";
		$stmt = mysqli_stmt_init($conn);
		if (!mysqli_stmt_prepare($stmt, $sql)) {
			header("Location: ../index.php?error=sqlerror");
			exit();
		}
		else {
			mysqli_stmt_bind_param($stmt, "ss", $mailuid, $mailuid);
			mysqli_stmt_execute($stmt);
			$result = mysqli_stmt_get_result($stmt);
			if ($row = mysqli_fetch_assoc($result)) {
				$pwdCheck = password_verify($password, $row['pwdUser']);
				if ($pwdCheck == false) {
					header("Location: ../index.php?error=wrongpassword");
					exit();
				}
				elseif ($pwdCheck == true) {
					session_start();
					$_SESSION['empKey'] = $row['employeeKey'];
					$_SESSION['userKey'] = $row['userKey'];
					$_SESSION['userUid'] = $row['uidUser'];
					$_SESSION['manager'] = $row['manager'];

$conn = new mysqli($servername, $username, $password, $dbHame);
$sql = "SELECT * FROM employees where employeeKey=".$_SESSION['empKey'].";";

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    }
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
        while($row = $result->fetch_assoc()) {
		   if ($row["employeeKey"] == $_SESSION['empKey'])
			   $_SESSION['city'] = $row['cityId'];
	}
}else { $_SESSION['city']=$result;
}
$conn->close();


					header("Location: ../index.php?login=success");
					exit();
				}
				else {
					header("Location: ../index.php?error=wrongpassword");
					exit();
				}
			}
			else {
				header("Location: ../index.php?error=nouser");
				exit();
			}
		}
	}
}
else {
	header("Location: ../index.php");
	exit();
}

?>
