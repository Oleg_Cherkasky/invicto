<?php require 'head.php' ;?>
<?php   if (!isset($_SESSION['userKey']))
		header("Location: index.php");
	else {
		$qlist = 6;
	}
?>
<main>

<form action="includes/hour.sel.php" method="post">
	<select name="employee-key" size=10>
	<?php
	require 'includes/dbh.inc.php';
	$sql =  "SELECT employeeKey, employeeName FROM employees;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
        echo "<option value=\"".$row['employeeKey']."\">".$row['employeeName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין עובדים</option>";

	$conn->close();
	?>
	</select>
	<br><button name="select-user" type="submit">לבחור משתמש</button><br>
</form>

<form action="includes/hour.sel.php" method="post" <?php if(!isset($_GET['query'])) echo "hidden"; ?>>

<?php

	if (isset($_GET['query'])) {
	   	$employee_key = $_GET['query'];
		require 'includes/list-queries.php';
		if (sizeof($listE) > 0) {
		for($i=0;count($listE)>$i;$i++) {
				$h_start = new DateTime($listE[$i][3]);
				$h_end = new DateTime($listE[$i][4]);
				echo "<p>תאריך: ".$listE[$i][2]." שעות עבודה:".$h_start->diff($h_end, true)->h."</p>";
				echo "<button name=\"h-delete\" type=\"submit\" value=".$listE[$i][0]." >למחוק</button>";
		     echo "<button name=\"h-edit\" type=\"submit\" value=".$listE[$i][0]." >לערוך</button><br>";
		}
		} else // no data for this user
			echo "אין שעות אצל העובד במאגר";
	}

	?>
</form>
</main>
<?php require 'foot.php' ;?>