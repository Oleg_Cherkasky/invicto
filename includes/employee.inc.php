<?php

if (isset($_POST['employee-submit'])) {
  require "dbh.inc.php";
  
  $wname = $_POST['wname'];
  $wbirth = $_POST['wbirth'];
  $wstartd = $_POST['wstartdate'];
  $wsalary = (int)$_POST['wsalary'];
  $profession = (int)$_POST['positions'];
  $city = (int)$_POST['cities'];
  $department = (int)$_POST['departments'];
  $globalp = "false";
  if ((int)$_POST['globalpay'])
     $globalp = "true";
  
  if (empty($wname) || empty($wbirth) || empty($wstartd)) {
	  header("Location: ../employee-input.php?error=emptyfields");
	  exit();
  }//-----------------
  elseif (!preg_match("/\p{Hebrew}/u", $wname)) {
	  header("Location: ../employee-input.php?error=invalidwname&wname=".$wname);
	  exit();
  }
  
  elseif (!preg_match("/^\d{4}[\s.\-][0-1][0-9][\s.\-][0-3][0-9]$/", $wbirth)) {
	  header("Location: ../employee-input.php?error=invalidphone&wbirth=".$wbirth);
	  exit();
  }
  elseif (!preg_match("/^\d{4}[\s.\-][0-1][0-9][\s.\-][0-3][0-9]$/", $wstartd)) {
	  header("Location: ../employee-input.php?error=invalidcity&wstartd=".$wstartd);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $wsalary)) {
	  header("Location: ../employee-input.php?error=invalidmanager&dmanager=".$wsalary);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $profession)) {
	  header("Location: ../employee-input.php?error=invalidsecretary&profession=".$profession);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $city)) {
	  header("Location: ../employee-input.php?error=invalidsecretary&city=".$city);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $department)) {
	  header("Location: ../employee-input.php?error=invalidsecretary&department=".$department);
	  exit();
  }
  else {
	$sql = "SELECT employeeName FROM employees WHERE employeeName=? AND birthDate=?;";
	$stmt = mysqli_stmt_init($conn);
	if (!mysqli_stmt_prepare($stmt, $sql)) {
		header("Location: ../employee-input.php?error=sqlerror");
		exit();
	}
	else {
		mysqli_stmt_bind_param($stmt,"ss", $wname, $wbirth);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_store_result($stmt);
		$resultCheck = mysqli_stmt_num_rows($stmt);
		if ($resultCheck > 0) {
			header("Location: ../employee-input.php?error=identicalmanagersecretary");
			exit();
		}
		else {
				$sql = "INSERT INTO employees (employeeName, birthDate, beginDate, salary, professionKey,deptKey, cityId, globalPay) VALUES (?, ?, ?, ?, ?, ?, ?, ".$globalp.");";
				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_bind_param($stmt,"sssiiii", $wname, $wbirth, $wstartd, $wsalary, $profession, $department, $city);
				if (!mysqli_stmt_prepare($stmt, $sql)) {
						header("Location: ../employee-input.php?error=sqlerror");
						exit();
				}
				else {
					mysqli_stmt_bind_param($stmt,"sssiiii", $wname, $wbirth, $wstartd, $wsalary, $profession, $department, $city);
					mysqli_stmt_execute($stmt);
					header("Location: ../employee-input.php?signup=success");
					exit();
				}
		}
	}
	mysqli_stmt_close($stmt);
	mysql_close($conn);
  }
}
else {
	header("Location: ../employee-input.php");
	exit();
	
}
?>