
<?php require 'head.php';?>

<main>
  <h2>Signup</h2>
  <?php
  
  if (!isset($_SESSION['userKey']))
	header("Location: index.php");
  
    if (isset($_GET['error'])) {
	  if ($_GET['error'] == "emptyfields") {
		echo "<p>Fill in all fields!</p>";
	  } elseif ($_GET['error'] == "invaliduidmail") {
		echo "<p>Invalid username and e-mail!</p>";
	  } elseif ($_GET['error'] == "invaliduid") {
		echo "<p>Invalid username!</p>";
	  } elseif ($_GET['error'] == "invalidmail") {
		echo "<p>Invalid e-mail!</p>";
	  } elseif ($_GET['error'] == "passwordcheck") {
		echo "<p>Your passwords do not match!</p>";
	  } elseif ($_GET['error'] == "usertaken") {
		echo "<p>Username already taken!</p>";
	  }
	} elseif (isset($_GET['success'])) {
	  echo "<p>Signup successful!</p>";
	}

  ?>
  <form action="includes/city.inc.php" method="post">
    <input type="text" name="cname" placeholder="שם העיר">
	
	
	
	</br />
    <button type="submit" name="city-submit">Signup</button>
  </form>
</main>

<?php require 'foot.php';?>