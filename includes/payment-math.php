<?php
function yearlyTax($p) {
	if($p < 75480)
		return $p - ($p * 0.10);
	elseif ($p > 75480)
	       return $p - ($p * 0.14);
	elseif ($p > 108360)
	       return $p - ($p * 0.20);
	elseif ($p > 173880)
	       return $p - ($p * 0.31);
	elseif ($p > 241680)
	       return $p - ($p * 0.35);
	elseif ($p > 502920)
	       return $p - ($p * 0.47);
	elseif ($p > 647640)
	       return $p - ($p * 0.50);
}

?>