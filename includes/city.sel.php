<?php
require "dbh.inc.php";

if (isset($_POST['cancel'])) {
	 header("Location: ../city-list.php");
	 exit();
}

if (isset($_POST['city-edit'])) {
	 header("Location: ../city-edit.php?query=".$_POST['city-edit']);
	 exit();
}
if (isset($_POST['city-delete'])) {
    	 $key = $_POST['city-delete'];
	 //echo $key;
	 $sql = "DELETE FROM cities WHERE cityId=".$key.";";
	 if ($conn->connect_error) {
	     header("Location: ../city-list.php?error=".$conn->connect_error);
	     $conn->close();
	     exit();
	 }
	 $result = $conn->query($sql);
	 $conn->close();
	 header("Location: ../city-list.php?status=deleted");
	 exit();

}

if (isset($_POST['city-update'])) {
  $cname = $_POST['cname'];
  $ckey = (int)$_POST['editid'];
  
  
  if (empty($cname)) {
	  header("Location: ../city-list.php?error=emptyfields");
	  exit();
  }
  elseif (!preg_match("/\p{Hebrew}/u", $cname)) {
	  header("Location: ../city-list.php?error=invalidpname&pname=".$cname);
	  exit();
  }
  else {
	
				$sql = "UPDATE cities SET cityName = ? WHERE cityId = ?;";
				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_bind_param($stmt,"si", $cname, $ckey);
				if (!mysqli_stmt_prepare($stmt, $sql)) {
						header("Location: ../city-list.php?query=".$deptKey."&error=sqlerror");
						exit();
				}
				else {
					mysqli_stmt_bind_param($stmt,"si", $cname, $ckey);
					mysqli_stmt_execute($stmt);
					header("Location: ../city-list.php?query=".$deptKey."&edit=successful-edit");
					exit();
				}
	
	mysqli_stmt_close($stmt);
	mysql_close($conn);

}
}

?>