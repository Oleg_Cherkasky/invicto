<?php
require "dbh.inc.php";

if (isset($_POST['cancel'])) {
   	 header("Location: ../user-list.php");
	 exit();
}

if (isset($_POST['user-edit'])) {
	 header("Location: ../user-edit.php?query=".$_POST['user-edit']);
	 exit();
}
if (isset($_POST['user-delete'])) {
   	 $key = $_POST['user-delete'];
	 //echo $key;
	 $sql = "DELETE FROM adminusers WHERE userKey=".$key.";";
	 if ($conn->connect_error) {
	     header("Location: ../user-list.php?error=".$conn->connect_error);
	     $conn->close();
	     exit();
	 }
	 $result = $conn->query($sql);
	 $conn->close();
	 header("Location: ../user-list.php?status=deleted");
	 exit();

}

if (isset($_POST['user-update'])) {
  
  $username = $_POST['uid'];
  $email = $_POST['mail'];
  $password = $_POST['pwd'];
  $passwordRepeat = $_POST['pwd-repeat'];
  $ekey = (int)$_POST['editid'];

if (empty($username) || empty($email) || empty($password) || empty($passwordRepeat)) {
	  header("Location: ../user-edit.php?query=".$ekey."&error=emptyfields&uid=".$username."&mail=".$email);
	  exit();
  }
  elseif (!filter_var($email,FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $username)) {
	  header("Location: ../user-edit.php?query=".$ekey."&error=invalidmailuid");
	  exit();
  }
  elseif (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
	  header("Location: ../user-edit.php?query=".$ekey."&error=invalidmailfield&uid=".$username);
	  exit();
  }
  elseif (!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
	  header("Location: ../user-edit.php?query=".$ekey."&error=invalidfields&mail=".$email);
	  exit();
  }
  elseif ($password !== $passwordRepeat) {
	  header("Location: ../user-edit.php?query=".$ekey."&error=passwordcheck&uid=".$username."&mail=".$email);
	  exit();
  }
  else {
				$sql = "UPDATE adminusers SET uidUser = ?, emailUser = ?, pwdUser = ? WHERE userKey = ?;";
				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_bind_param($stmt,"sssi", $username, $email, password_hash($password, PASSWORD_DEFAULT), $ekey);
				if (!mysqli_stmt_prepare($stmt, $sql)) {
						header("Location: ../user-edit.php?query=".$ekey."&error=sqlerror");
						exit();
				}
				else {
					
					mysqli_stmt_bind_param($stmt,"sssi", $username, $email, password_hash($password, PASSWORD_DEFAULT), $ekey);
					mysqli_stmt_execute($stmt);

					header("Location: ../user-list.php?signup=success");
					exit();
				}
		
		
	}
	mysqli_stmt_close($stmt);
	mysql_close($conn);  
  
}

?>