<?php require 'head.php' ;
        if (!isset($_SESSION['userKey']))
		header("Location: index.php");
	if((!isset($_GET['query'])) || $_GET['query'] == '') {
	     header("Location: hours-list.php");
	     exit();
	}
	else {
	     $qtype = 6;
	     $query = $_GET['query'];
     	     require "includes/fetch.php";
	}
?>
<main>
  <h2>Signup</h2>
  <?php  
    if (isset($_GET['error'])) {
	  if ($_GET['error'] == "emptyfields") {
		echo "<p>Fill in all fields!</p>";
	  } elseif ($_GET['error'] == "invaliduidmail") {
		echo "<p>Invalid username and e-mail!</p>";
	  } elseif ($_GET['error'] == "invaliduid") {
		echo "<p>Invalid username!</p>";
	  } elseif ($_GET['error'] == "invalidmail") {
		echo "<p>Invalid e-mail!</p>";
	  } elseif ($_GET['error'] == "passwordcheck") {
		echo "<p>Your passwords do not match!</p>";
	  } elseif ($_GET['error'] == "usertaken") {
		echo "<p>Username already taken!</p>";
	  }
	} elseif (isset($_GET['success'])) {
	  echo "<p>Signup successful!</p>";
	}

  ?>
  <form action="includes/hour.sel.php" method="post">
    <input type="text" name="date" value="<?php echo $listE[2] ?>">
    <input type="text" name="hstart" value="<?php echo $listE[0] ?>">
	<input type="text" name="hend" value="<?php echo $listE[1] ?>">
	
	<select name="people">
	<?php
	$sql =  "SELECT employeeKey, employeeName FROM employees;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
	  	if($listE[3] == $row['employeeKey'])
		        echo "<option value=\"".$row['employeeKey']."\" selected>".$row['employeeName']."</option>";
		else
			echo "<option value=\"".$row['employeeKey']."\">".$row['employeeName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין מקצועות</option>";
	$conn->close();
	?>
	</select>
	
	</br />
    <input type="hidden" name="editid" value="<?php echo $listE[4] ?>">
    <button type="submit" name="hour-submit">Signup</button>
  </form>
</main>

<?php require 'foot.php';?>