<?php

if (isset($_POST['signup-submit'])) {
  require "dbh.inc.php";
  
  $username = $_POST['uid'];
  $email = $_POST['mail'];
  $password = $_POST['pwd'];
  $passwordRepeat = $_POST['pwd-repeat'];
  $worker = (int)$_POST['employee-key'];
  $manager = (int)$_POST['manager'];
  
  
  if (empty($username) || empty($email) || empty($password) || empty($passwordRepeat)) {
	  header("Location: ../signup.php?error=emptyfields&uid=".$username."&mail=".$email);
	  exit();
  }
  elseif (!filter_var($email,FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $username)) {
	  header("Location: .. /signup.php?error=invalidmailuid");
	  exit();
  }
  elseif (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
	  header("Location: ../signup.php?error=invalidmailfield&uid=".$username);
	  exit();
  }
  elseif (!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
	  header("Location: ../signup.php?error=invalidfields&mail=".$email);
	  exit();
  }
  elseif ($password !== $passwordRepeat) {
	  header("Location: ../signup.php?error=passwordcheck&uid=".$username."&mail=".$email);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $worker)) {
	  header("Location: .. /department-input.php?error=doesnotexist&worker=".$worker);
	  exit();
  }
  else {
	$sql = "SELECT uidUser FROM adminusers WHERE uidUser=? OR emailUser=?";
	$stmt = mysqli_stmt_init($conn);
	if (!mysqli_stmt_prepare($stmt, $sql)) {
		header("Location: ../signup.php?error=sqlerror");
		exit();
	}
	else {
		mysqli_stmt_bind_param($stmt,"ss", $username, $email);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_store_result($stmt);
		$resultCheck = mysqli_stmt_num_rows($stmt);
		if ($resultCheck > 0) {
			header("Location: ../signup.php?error=userandemailtaken");
			exit();
		}
		else {
				$sql = "INSERT INTO adminusers (uidUser, manager, emailUser, pwdUser, employeeKey) VALUES (?, ?, ?, ?, ?);";
				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_bind_param($stmt,"sissi", $username, $manager, $email, password_hash($password, PASSWORD_DEFAULT), $worker);
				if (!mysqli_stmt_prepare($stmt, $sql)) {
						header("Location: ../signup.php?error=sqlerror");
						exit();
				}
				else {
					mysqli_stmt_bind_param($stmt,"sissi", $username, $manager, $email, password_hash($password, PASSWORD_DEFAULT), $worker);
					mysqli_stmt_execute($stmt);
					header("Location: ../user-list.php?signup=success");
					exit();
				}
		}
	}
	mysqli_stmt_close($stmt);
	mysql_close($conn);
  }
}
else {
	header("Location: ../signup.php");
	exit();
}

?>
