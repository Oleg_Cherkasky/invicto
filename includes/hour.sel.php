<?php
require "dbh.inc.php";

if (isset($_POST['calc-user'])) {
	 header("Location: ../payment-calc.php?query=".$_POST['employee-key']);
	 exit();
}

if (isset($_POST['h-edit'])) {
	 header("Location: ../hours-edit.php?query=".$_POST['h-edit']);
	 exit();
}

if (isset($_POST['select-user'])) {
   	 if (isset($_POST['employee-key'])) {
	    header("Location: ../hours-list.php?query=".$_POST['employee-key']);
	    exit();
	 } else {
	   header("Location: ../hours-list.php");
	   exit();
	 }
}

if (isset($_POST['h-delete'])) {
   	 $key = $_POST['h-delete'];
	 //echo $key;
	 $sql = "DELETE FROM hours WHERE hoursKey=".$key.";";
	 if ($conn->connect_error) {
	     header("Location: ../hours-list.php?error=".$conn->connect_error);
	     $conn->close();
	     exit();
	 }
	 $result = $conn->query($sql);
	 $conn->close();
	 header("Location: ../hours-list.php?query=".$key."&status=deleted");
	 exit();

}

if (isset($_POST['cancel'])) {
	 header("Location: ../hours-list.php");
	 exit();
}

if (isset($_POST['h-update'])) {
  $hdate = $_POST['date'];
  $start = $_POST['hstart'];
  $end = $_POST['hend'];
  $person = (int)$_POST['people'];
  
  
  if (empty($hdate) || empty($start) || empty($end)) {
	  header("Location: ../hours-input.php?error=emptyfields");
	  exit();
  }
  elseif (!preg_match("/^[0-2][0-9]\:[0-5][0-9]$/", $start)) {
	  header("Location: ../hours-input.php?error=invalidtimestart&start=".$start);
	  exit();
  }
    elseif (!preg_match("/^[0-2][0-9]\:[0-5][0-9]$/", $end)) {
	  header("Location: ../hours-input.php?error=invalidtimeend&end=".$end);
	  exit();
  }
  elseif ((round(abs(strtotime($end) - strtotime($start)) / 3600,2))< 1) {
	  header("Location: ../hours-input.php?error=negativehours&start=".$start."&end=".$end);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $person)) {
	  header("Location: ../hours-input.php?error=invalidperson&person=".$person);
	  exit();
  }
  else {
	$sql = "SELECT * FROM hours WHERE employeeKey=? AND workDate=? AND startHour=?;";
	$stmt = mysqli_stmt_init($conn);
	if (!mysqli_stmt_prepare($stmt, $sql)) {
		header("Location: ../hours-input.php?error=sqlerror");
		exit();
	}
	else {
		mysqli_stmt_bind_param($stmt,"iss", $person, $hdate, $start);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_store_result($stmt);
		$resultCheck = mysqli_stmt_num_rows($stmt);
		if ($resultCheck > 0) {
			header("Location: ../hours-input.php?error=identicalhours");
			exit();
		}
		else {
			    $total = round(abs(strtotime($end) - strtotime($start)) / 3600,2);
				$sql = "INSERT INTO hours (employeeKey, workDate, startHour, endHour, totalHours) VALUES (?, ?, ?, ?, ?);";
				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_bind_param($stmt,"isssi", $person, $hdate, $start, $end, $total);
				if (!mysqli_stmt_prepare($stmt, $sql)) {
						header("Location: ../hours-list.php?error=sqlerror");
						exit();
				}
				else {
					mysqli_stmt_bind_param($stmt,"isssi", $person, $hdate, $start, $end, $total);
					mysqli_stmt_execute($stmt);
					header("Location: ../hours-list.php?signup=success");
					exit();
				}

		}
}	
	mysqli_stmt_close($stmt);
	mysql_close($conn);

}
}

?>