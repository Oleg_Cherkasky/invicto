
<?php require 'head.php';?>

<main>
  <h2>Signup</h2>
  <?php
  
  if (!isset($_SESSION['userKey']))
	header("Location: index.php");
  
    if (isset($_GET['error'])) {
	  if ($_GET['error'] == "emptyfields") {
		echo "<p>Fill in all fields!</p>";
	  } elseif ($_GET['error'] == "invaliduidmail") {
		echo "<p>Invalid username and e-mail!</p>";
	  } elseif ($_GET['error'] == "invaliduid") {
		echo "<p>Invalid username!</p>";
	  } elseif ($_GET['error'] == "invalidmail") {
		echo "<p>Invalid e-mail!</p>";
	  } elseif ($_GET['error'] == "passwordcheck") {
		echo "<p>Your passwords do not match!</p>";
	  } elseif ($_GET['error'] == "usertaken") {
		echo "<p>Username already taken!</p>";
	  }
	} elseif (isset($_GET['success'])) {
	  echo "<p>Signup successful!</p>";
	}

  ?>
  <form action="includes/employee.inc.php" method="post">
    <input type="text" name="wname" placeholder="שם מלא">
    <span> תאריך לידה:</span><input type="date" name="wbirth" >
	<span> תאריך התחלת עבודה:</span><input type="date" name="wstartdate" >
	<input type="text" name="wsalary" placeholder="שכר">
	
	<span><br />מקצועות:</span>
	<select name="positions">
	<?php
	require 'includes/dbh.inc.php';
	$sql =  "SELECT professionKey, profession FROM professions;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
        echo "<option value=\"".$row['professionKey']."\">".$row['profession']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין מקצועות</option>";

	?>
	</select>
	
	<span>ערים:</span>
	<select name="cities">
	<?php
	$sql =  "SELECT cityId, cityName FROM cities;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
	          echo "<option value=\"".$row['cityId']."\">".$row['cityName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין ערים</option>";

	?>
	</select>
	
	<span>מחלקות:</span>
	<select name="departments">
	<?php
	$sql =  "SELECT deptKey, deptName FROM departments;";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
        echo "<option value=\"".$row['deptKey']."\">".$row['deptName']."</option>";
	  }
	} else
		echo "<option value=\"0\">אין מחלקות</option>";
	$conn->close();
	?>
	</select>

	<span>משכורת גלובאלית:</span>
	<select name="globalpay">
		<option value=1>כן</option>
		<option value=0>לא</option>
	</select>
	
	</br />
    <button type="submit" name="employee-submit">Signup</button>
  </form>
</main>

<?php require 'foot.php';?>