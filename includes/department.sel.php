<?php
require "dbh.inc.php";

if (isset($_POST['dept-edit'])) {
	 header("Location: ../department-edit.php?query=".$_POST['dept-edit']);
	 exit();
}
if (isset($_POST['dept-delete'])) {
   	 $key = $_POST['dept-delete'];
	 //echo $key;
	 $sql = "DELETE FROM departments WHERE deptKey=".$key.";";
	 if ($conn->connect_error) {
	     header("Location: ../department-list.php?error=".$conn->connect_error);
	     $conn->close();
	     exit();
	 }
	 $result = $conn->query($sql);
	 $conn->close();
	 header("Location: ../department-list.php?status=deleted");
	 exit();

}

if (isset($_POST['cancel'])) {
	 header("Location: ../department-list.php");
	 exit();
}

if (isset($_POST['department-update'])) {
  $deptkey = (int)$_POST['editid'];
  $dname = $_POST['dname'];
  $sphone = $_POST['secretaryphone'];
  $dcities = (int)$_POST['cities'];
  $dmanager = (int)$_POST['manager'];
  $dsecretary = (int)$_POST['secretary'];
  
  
  if (empty($dname) || empty($sphone) || empty($dcities) || empty($dmanager) || empty($dsecretary)) {
	  header("Location: ../department-input.php?error=emptyfields");
	  exit();
  }
  elseif (!preg_match("/\p{Hebrew}/u", $dname)) {
	  header("Location: ../department-input.php?error=invaliddname&dname=".$dname);
	  exit();
  }
  elseif (!preg_match("/^\d{3}\)?[\s.-]\d{7}$/", $sphone)) {
	  header("Location: ../department-input.php?error=invalidphone&sphone=".$sphone);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $dcities)) {
	  header("Location: ../department-input.php?error=invalidcity&dcities=".$dcities);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $dmanager)) {
	  header("Location: ../department-input.php?error=invalidmanager&dmanager=".$dmanager);
	  exit();
  }
  elseif (!preg_match("/^(?!0+$)\d+$/", $dsecretary)) {
	  header("Location: ../department-input.php?error=invalidsecretary&dsecretary=".$dsecretary);
	  exit();
  }
  else {
	
				$sql = "UPDATE departments SET deptName = ?, secretaryPhone = ?, cityId = ?, managerKey = ?, secretaryKey = ? WHERE deptKey = ?;";
				$stmt = mysqli_stmt_init($conn);
				mysqli_stmt_bind_param($stmt,"ssiiii", $dname, $sphone, $dcities, $dmanager, $dsecretary, $deptkey);
				if (!mysqli_stmt_prepare($stmt, $sql)) {
						header("Location: ../department-edit.php?query=".$deptKey."&error=sqlerror");
						exit();
				}
				else {
					mysqli_stmt_bind_param($stmt,"ssiiii", $dname, $sphone, $dcities, $dmanager, $dsecretary, $deptkey);
					mysqli_stmt_execute($stmt);
					header("Location: ../department-list.php?query=".$deptKey."&edit=successful-edit");
					exit();
				}
	
	mysqli_stmt_close($stmt);
	mysql_close($conn);

}
}

?>